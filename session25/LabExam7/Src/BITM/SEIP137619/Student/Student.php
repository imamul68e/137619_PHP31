<?php
namespace App\BITM\SEIP137619\Student;
use PDO;
class Student
{
    public $fName="";
    public $lName="";
    public $nickName="";
    public $pass="bitm";
    public $dbName="labexam7";
    public $user="bitm";
    public $host="localhost";
    public $conn;

    public function __construct()
    {
        $hostName=$this->host;
        $datbName=$this->dbName;
        $userName=$this->user;
        $password=$this->user;
        try {
            $this->conn = new PDO("mysql:host=$hostName;dbname=$datbName", $userName, $password);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function setData($getInfo=''){
        if(array_key_exists('fName',$getInfo) and !empty($getInfo)){
            $this->fName=$getInfo['fName'];
        }
        if(array_key_exists('lName',$getInfo) and !empty($getInfo)){
            $this->lName=$getInfo['lName'];
        }
        if(array_key_exists('nName',$getInfo) and !empty($getInfo)){
            $this->nickName=$getInfo['nName'];
        }
        return $this;
    }
    public function store(){
        $query="INSERT INTO `student` (`firstName`, `lastName`, `nickName`) VALUES (:FirstName, :Lastname, :NickName)";
        $getresult= $this->conn->prepare($query);
        $result=$getresult->execute(array(':FirstName'=>$this->fName, ':Lastname'=>$this->lName, ':NickName'=>$this->nickName));
        if ($result){
            echo "Data inserted successfully.";
        }
        else{
            echo "Data not inserted.";
        }
    }

}