<?php
session_start();
//echo $_SERVER['DOCUMENT_ROOT'];
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."AtomicProjectB31".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");


use App\Bitm\SEIPXXXX\Mobile\Mobile;
use App\Bitm\SEIPXXXX\Message\Message;

$obj= new Mobile();
$totalItem=$obj->count();
//echo $totalItem;


//App\Bitm\SEIPXXXX\Utility\Utility::dd($allResult);
//Pagination Code
if(array_key_exists('itemPerPage',$_SESSION)){
    if(array_key_exists('itemPerPage',$_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}

else{
    $_SESSION['itemPerPage']=5;
}


$itemPerPage=$_SESSION['itemPerPage'];

//echo $itemPerPage;


$totalPage=ceil($totalItem/$itemPerPage);
//echo $totalPage;
$paginate="";
if(array_key_exists('pageNumber',$_GET)){
    $pageNumber= $_GET['pageNumber'];
}
else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class = ( $pageNumber == $i ) ? "active" : "";
    $paginate.="<li class='$class'><a href='index.php?pageNumber=".$i."'>".$i."</a></li>";
}



$pageStartFrom=$itemPerPage*($pageNumber-1);
if(array_key_exists('pageNumber',$_GET)) {
    $prevPage = $_GET['pageNumber'] - 1;
}
if(array_key_exists('pageNumber',$_GET)) {
    $nextPage = $_GET['pageNumber'] + 1;
}

$allResult=$obj->paginator($pageStartFrom,$itemPerPage);



?>


<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
    </br>
    </br>
    <div id="message">
        <?php
        if(array_key_exists('success_message',$_SESSION) and !empty($_SESSION['success_message'])){
            echo Message::message();
        }
        ?>
    </div>
    <form action="index.php" method="get">
    <div class="form-group">
        <label for="sel1">Select number of items you want to view</label>
        <select class="form-control" id="sel1" name="itemPerPage">
            <option>5</option>
            <option>10</option>
            <option>15</option>
            <option>20</option>
        </select>
        <br>
        <button type="submit" class="btn btn-info">Go!</button>
    </div>
    </form>

    <a href="create.php" class="btn btn-primary" role="button">Create Again</a>
    <a href="trashed.php" class="btn btn-success" role="button"> All trashed list</a>
    <a href="pdf.php" class="btn btn-info" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
    <a href="mail.php" class="btn btn-success" role="button">Mail to friend</a>



    <h2>All Mobile List</h2>
    <ul class="pagination">
        <?php
        if(array_key_exists('pageNumber',$_GET)) {
            if (($_GET['pageNumber']) > 1) {
                echo "<li><a href=index.php?pageNumber=" . $prevPage . ">Previous</a></li>";
            }
        }?>
        <?php echo $paginate?>

        <?php  if(array_key_exists('pageNumber',$_GET)) {
            if (($_GET['pageNumber'])<$totalPage) {
                echo "<li><a href=index.php?pageNumber=" . $nextPage . ">Next</a></li>";
            }
        }?>

    </ul>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Mobile Name</th>
            <th>Model</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl=0;
        foreach($allResult as $result):
            $sl++?>
            <tr>
                <td><?php echo $sl?></td>
                <td><?php echo $result->id ?></td>
                <td><?php echo $result->title ?></td>
                <td><?php echo $result->model_name ?></td>
                <td> <a href="view.php?id=<?php echo $result->id?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $result->id?>" class="btn btn-primary" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $result->id?>" class="btn btn-danger" role="button">Delete</a>
                    <a href="trash.php?id=<?php echo $result->id?>" class="btn btn-primary" role="button">Trash</a>

                    <!--                    <form method="post" action="delete.php">-->
                    <!--                        <input type="hidden" name="id" value="--><?php //echo $result->id ?><!--" >-->
                    <!---->
                    <!--                        <button type="submit" Onclick="return ConfirmDelete()">Delete</button>-->
                    <!--                    </form>-->


                </td>
            </tr>
        <?php endforeach ?>

        </tbody>
    </table>

</div>
<script>
    $("#message").show().delay(3000).fadeOut();

    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>

</body>
</html>
