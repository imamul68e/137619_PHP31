<?php
$int=10;
$float=10.5;
$floatval="123565.52imamul";
$bool1=1;
$bool2=false;
$str="My name is Imamul";
$empty2="";
$array=array("imamul", "hossain");

echo $int ." is ".is_int($int)."<br>";
echo $float ." is ".is_float($float)."<br>";
echo $bool1 ." is ".is_bool($bool1)."<br>";
echo $bool2 ." is ".is_bool($bool2)."<br>";
echo $str ." is ".is_string($str)."<br>";
echo $empty2 ." is ".is_string($empty2)."<br>";
echo $int ." is ".gettype($int)."<br>";
echo $float ." is ".gettype($float)."<br>";
echo $floatval ." has float value".floatval($floatval)."<br>";
echo $bool1 ." is ".gettype($bool1)."<br>";
echo $bool2 ." is ".gettype($bool2)."<br>";
echo $str ." is ".gettype($str)."<br>";
echo $empty2 ." is ".gettype($empty2)."<br>";
echo "It is ".gettype($array)."<br>";

echo "<pre>";

var_dump($array);
print_r($array);

var_dump(serialize($array));
print_r(unserialize(serialize($array)));

