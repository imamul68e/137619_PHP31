<?php
namespace App\BITM\SEIP137619\Message;
if (!isset($_SESSION['giveMessage'])) {
    session_start();
}

class Message
{
    public static function Message($message = NULL)
    {
        if (is_null($message)) {
            $_message = self::getMessage();
            return $_message;
        } else {
            self::setMessage($message);
        }
    }

    public static function setMessage($msg)
    {
        $_SESSION['giveMessage'] = $msg;
    }

    public static function getMessage()
    {
        $_message = $_SESSION['giveMessage'];
        $_SESSION['giveMessage'] = "";
        return $_message;
    }
}