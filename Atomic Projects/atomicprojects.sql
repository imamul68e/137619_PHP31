-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2016 at 06:38 AM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicprojects`
--

-- --------------------------------------------------------

--
-- Table structure for table `alldistrict`
--

CREATE TABLE `alldistrict` (
  `id` int(11) NOT NULL,
  `district` varchar(255) NOT NULL,
  `isTrash` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alldistrict`
--

INSERT INTO `alldistrict` (`id`, `district`, `isTrash`) VALUES
(1, 'Barguna', 0),
(2, 'Barisal', 0),
(3, 'Bhola', 0),
(4, 'Jhalokati', 0),
(5, 'Patuakhali ', 0),
(6, ' Pirojpur ', 0),
(7, 'Bandarban ', 0),
(8, 'Brahmanbaria', 0),
(9, 'Chandpur ', 0),
(10, 'Chittagong ', 0),
(11, 'Comilla ', 0),
(12, 'Cox\'s Bazar ', 0),
(13, 'Feni ', 0),
(14, 'Khagrachhari ', 0),
(15, 'Lakshmipur ', 0),
(17, 'Noakhali', 0),
(18, 'Rangamati ', 0),
(19, 'Dhaka ', 0),
(20, 'Faridpur ', 0),
(21, 'Gazipur ', 0),
(22, 'Gopalganj ', 0),
(23, 'Jamalpur ', 0),
(24, 'Kishoreganj ', 0),
(25, 'Madaripur ', 0),
(26, 'Manikganj ', 0),
(27, 'Munshiganj ', 0),
(28, 'Mymensingh ', 0),
(29, 'Narayanganj ', 0),
(30, 'Narsingdi ', 0),
(31, 'Netrakona ', 0),
(32, ' Rajbari', 0),
(33, 'Shariatpur ', 0),
(34, ' Sherpur ', 0),
(35, 'Tangail ', 0),
(36, 'Bagerhat ', 0),
(37, 'Chuadanga ', 0),
(38, 'Jessore ', 0),
(39, 'Jhenaidah ', 0),
(40, 'Khulna ', 0),
(41, 'Kushtia ', 0),
(42, 'Magura ', 0),
(43, ' Meherpur ', 0),
(44, 'Narail ', 0),
(45, 'Satkhira ', 0),
(46, 'Bogra ', 0),
(47, 'Joypurhat ', 0),
(48, 'Naogaon ', 0),
(49, 'Natore ', 0),
(50, 'Nawabganj ', 0),
(51, 'Pabna', 0),
(52, 'Rajshahi ', 0),
(53, ' Sirajganj ', 0),
(54, 'Dinajpur ', 0),
(55, 'Gaibandha ', 0),
(56, 'Kurigram ', 0),
(57, 'Lalmonirhat ', 0),
(58, 'Nilphamari ', 0),
(59, 'Panchagarh ', 0),
(60, 'Rangpur ', 0),
(61, 'Thakurgaon ', 0),
(62, ' Habiganj ', 0),
(63, 'Moulvibazar ', 0),
(64, ' Sunamganj ', 0),
(65, 'Sylhet ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `isTrash` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`, `isTrash`) VALUES
(1, 'Imamul', '2016-11-10', 0),
(5, 'dss', '2023-05-07', 0),
(4, 'Imamul', '2016-11-18', 0),
(6, 'shuvo', '2012-11-04', 0),
(7, 'imamulgdg', '2012-11-04', 0),
(8, 'imamul', '2012-11-04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(20) NOT NULL,
  `isTrash` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `isTrash`) VALUES
(2, 'IEB ', 'Dhaka', 0),
(3, 'imamul', 'Satkhira', 0),
(9, 'sumon', 'Khulna', 0),
(8, 'bitm', 'Dhaka ', 0),
(12, 'sumon', 'Barishal', 0),
(13, 'sumon', 'Dhaka', 0),
(14, 'imamult', 'Sylhet', 0),
(17, 'sir', 'Satkhira ', 0),
(16, 'IEB', 'Dhaka', 0);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `isTrash` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `companyName`, `description`, `isTrash`) VALUES
(3, 'bitm', '', 0),
(5, 'mnhgfbdsdfvgbhnjm', '234y6uuykjtre', 0),
(8, 'wert', 'retryukl;\'', 0),
(12, 'wert', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 0);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `isTrash` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `isTrash`) VALUES
(2, 'shuvo', 'shueevo@gmail.com', 1),
(3, 'milona', 'milon@yahoo.com', 1),
(5, 'ami', 'ami@yahoo.com', 1),
(6, 'bitm', 'bitm@yahoo.com', 1),
(9, 'IEB ', 'ieb@gmail.com', 1),
(13, 'nishi', 'nishi@gmia.com', 1),
(15, 'jjh', 'hhhhhhhh@ff.com', 1),
(16, 'ihih', 'hfih@hdu.coom', 1),
(17, 'ihdhhdhi', 'dj@hf.com', 1),
(18, 'ffgfg', 'gfg@f.b', 1),
(19, 'imamulfdf', 'ddfe@djk.com', 1),
(20, 'vfdvf', 'hhhhhhhh@ff.com', 1),
(21, 'babu', 'vfgg@fff.b', 1),
(22, 'guewwh', 'ddfh@hdf.com', 1),
(23, 'guewwh', 'ddfh@hdf.com', 1),
(24, 'gtgttg', 'ggtg@grg.b', 1),
(25, 'gtgttg', 'ggtg@grg.b', 1),
(61, 'babu', 'hhhhhhhh@ff.com', 1),
(29, 'ffg', 'fdf@f.d', 1),
(30, 'ffg', 'fdf@f.d', 1),
(31, 'ffg', 'fdf@f.d', 1),
(32, 'ffg', 'fdf@f.d', 1),
(33, 'ffg', 'fdf@f.d', 1),
(34, 'ffg', 'fdf@f.d', 1),
(35, 'imam', 'imamul68e@gmail.com', 1),
(36, 'babu', 'fdf@f.d', 1),
(37, 'imam', 'ggtg@grg.b', 1),
(38, 'imamul', 'ggtg@grg.b', 1),
(40, 'imamul', 'ddfhf@hdf.com', 0),
(41, 'IEB', 'ggtg@grg.b', 0),
(43, 'imamul', 'fdf@f.d', 0),
(44, 'babu', 'ddfh@hdf.com', 1),
(45, 'imamul', 'fdf@f.d', 1),
(46, 'imamul', 'hhhhhhhh@ff.com', 0),
(48, 'imamul', 'fdf@f.d', 0),
(49, 'imamul', 'fdf@f.d', 0),
(64, 'imamul', '', 0),
(51, 'imamul', 'ggtg@grg.b', 0),
(52, 'babu', 'ggtg@grg.b', 0),
(53, 'babu', 'fdf@f.d', 0),
(54, 'babu', 'ggtg@grg.b', 0),
(55, 'imamulg', 'imamul68e@gmail.com', 0),
(56, 'babu', 'ggtg@grg.b', 0),
(57, 'babu', 'hhhhhhhh@ff.com', 0),
(58, 'shuvo', 'imamul68e@gmail.com', 0),
(62, 'babu', 'ggtg@grg.b', 0),
(63, 'kamal', 'imamul68e@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `isTrash` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `isTrash`) VALUES
(1, 'madam', 'Female', 0),
(2, 'imamul', 'Male', 0),
(3, 'wajed', 'Other', 0),
(5, 'babu', 'Male', 0),
(7, 'amir', 'Other', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `hobby` varchar(255) NOT NULL,
  `isTrash` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `hobby`, `isTrash`) VALUES
(3, 'imamul', 'C#,Dot Net,PHP,HTML,CSS,Java Script', 0),
(4, 'imamul', 'C,C++,C#,Dot Net,PHP,HTML,CSS', 0),
(5, 'kamal', 'C#,PHP,HTML,CSS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mobile`
--

CREATE TABLE `mobile` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `isTrash` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile`
--

INSERT INTO `mobile` (`id`, `name`, `mobile`, `isTrash`) VALUES
(11, 'imamul', '765432', 0),
(5, 'gdh', '65432', 0),
(6, 'hdfghd', 'hdhdg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `profilephoto`
--

CREATE TABLE `profilephoto` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilephoto`
--

INSERT INTO `profilephoto` (`id`, `name`, `photo`) VALUES
(17, 'imamul Hossain', '1480008139MIST logo.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alldistrict`
--
ALTER TABLE `alldistrict`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile`
--
ALTER TABLE `mobile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilephoto`
--
ALTER TABLE `profilephoto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alldistrict`
--
ALTER TABLE `alldistrict`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mobile`
--
ALTER TABLE `mobile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profilephoto`
--
ALTER TABLE `profilephoto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
