<?php
include("../../../../vendor/autoload.php");
use App\BITM\SEIP137619\City\City;

$city = new City();
$allDistrict = $city->getDistrict();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>City Form</title>

    <!-- Bootstrap -->
    <link href="../../../../Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../../Assets/css/form.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="backgroundColor">
<section class="container">
    <br><br>
    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <form action="store.php" method="post">
                <fieldset>
                    <legend>Add City Info</legend>
                    <div class="row formBackground">
                        <div class="col-md-10 col-md-offset-1">
                            <br>
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="name" placeholder="Enter Name" class="form-control"
                                       required="required" autofocus="autofocus" id="name">
                            </div>
                            <div class="form-group">
                                <label for="city">City:</label>
                                <select class="form-control" id="city" name="city">
                                    <option>Select City</option>
                                    <?php
                                    foreach ($allDistrict as $district) {
                                        ?>
                                        <option
                                            value="<?php echo $district['district'] ?>"><?php echo $district['district'] ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                                    <button type="reset" class="btn btn-info btn-sm">Reset</button>
                                    <br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</section>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../../Assets/js/bootstrap.min.js"></script>
</body>
</html>