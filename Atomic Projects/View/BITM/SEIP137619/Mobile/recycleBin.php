<?php
include("../../../../vendor/autoload.php");
use App\BITM\SEIP137619\Mobile\Mobile;
use App\BITM\SEIP137619\Message\Message;

session_start();
$Mobile = new Mobile();
$allMobiles = $Mobile->recycleBin();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Recycle Bin</title>

    <!-- Bootstrap -->
    <link href="../../../../Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../../Assets/css/form.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="backgroundColor">
<section class="container">
    <br>
    <div class="row headerRow">
        <div class="col-md-1 col-md-offset-1">
            <a href="index.php" class="btn btn-primary" role="button">View All Data</a>
        </div>
        <div class="col-md-6 messageShow">
            <div id="message">
                <?php
                if (array_key_exists('giveMessage', $_SESSION) and !empty($_SESSION['giveMessage'])) {
                    echo Message::message();
                }
                ?>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <legend>Recycle Bin</legend>
            <table class="table table-hover table-striped tableClass">
                <thead>
                <th>Serial No</th>
                <th>ID No</th>
                <th>Name</th>
                <th>Mobile No</th>
                <th>Actions</th>
                </thead>

                <tbody>
                <?php
                $sl = 0;
                foreach ($allMobiles as $Mobile) {
                    $sl++;
                    ?>
                    <tr>
                        <td><?php echo $sl ?></td>
                        <td><?php echo $Mobile['id'] ?></td>
                        <td><?php echo $Mobile['name'] ?></td>
                        <td><?php echo $Mobile['mobile'] ?></td>
                        <td>
                            <form action="delete.php" method="post">
                                <a href="restore.php?id=<?php echo $Mobile['id']; ?>"
                                   class="btn btn-info btn-xs" role="button">Restore</a>
                                <input type="hidden" name="id" value="<?php echo $Mobile['id']; ?>">
                                <button type="submit" class="btn btn-danger btn-xs"
                                        Onclick="return ConfirmDelete();">Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
</section>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../../Assets/js/bootstrap.min.js"></script>
<script>
    $("#message").show().delay(3000).fadeOut();
    function ConfirmDelete() {
        var x = confirm("Are you sure to delete?");
        if (x)
            return true;
        else
            return false;
    }
</script>
</body>
</html>