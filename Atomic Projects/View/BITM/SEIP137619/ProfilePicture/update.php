<?php
include ("../../../../vendor/autoload.php");
use App\BITM\SEIP137619\ProfilePicture\profilePicture;
$file= new profilePicture();
$file->setData($_POST);
$singleData=$file->view();

if (isset($_FILES['photo']) and !empty($_FILES['photo']['name'])){
    $photoName=time().$_FILES['photo']['name'];
    $tempLocation=$_FILES['photo']['tmp_name'];
    unlink($_SERVER['DOCUMENT_ROOT']."/137619_PHP31/Atomic Projects/Assets/uploadedFile/".$singleData['photo']);
    move_uploaded_file($tempLocation,"../../../../Assets/uploadedFile/".$photoName);

    $_POST['photo']=$photoName;

}
$photo=new profilePicture();
$photo->setData($_POST);
$photo->update();
