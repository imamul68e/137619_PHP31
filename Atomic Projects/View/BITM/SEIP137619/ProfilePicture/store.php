<?php
include ("../../../../vendor/autoload.php");
use App\BITM\SEIP137619\ProfilePicture\profilePicture;

if (isset($_FILES['photo']) and !empty($_FILES['photo']['name'])){
    $photoName=time().$_FILES['photo']['name'];
    $tempLocation=$_FILES['photo']['tmp_name'];
    move_uploaded_file($tempLocation,"../../../../Assets/uploadedFile/".$photoName);

    $_POST['photo']=$photoName;


    $photo=new profilePicture();
    $photo->setData($_POST);
    $photo->store();

}



