<?php
namespace App\BITM\SEIP137619\District;
use App\BITM\SEIP137619\Message\Message;
use PDO;

class District
{
    public $id = "";
    public $district = "";
    public $host = "localhost";
    public $db = "atomicprojects";
    public $user = "atomic";
    public $pass = "123";
    public $conn;

    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db", $user = $this->user, $pass = $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data = "")
    {
        if (array_key_exists('district', $data) and !empty($data)) {
            $this->district = $data['district'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
    }

    public function store()
    {
        $sql = "INSERT INTO `alldistrict`(`district`, `isTrash`) VALUES (:myDistrict,:Trash)";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":myDistrict" => $this->district, ":Trash"=>0));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is inserted successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Data is not inserted successfully.
                                   </div>");
            header("Location: index.php");
        }
    }

    public function index()
    {
        $sql = "SELECT * FROM `alldistrict` WHERE `isTrash`=0";
        $getSql = $this->conn->query($sql);
        $result = $getSql->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function view()
    {
        $sql = "SELECT * FROM `alldistrict` WHERE `id`=:getId";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":getId" => $this->id));
        $singleResult = $getSql->fetch(PDO::FETCH_ASSOC);
        return $singleResult;
    }

    public function update()
    {
        $sql = "UPDATE `alldistrict` SET `district`=:setDistrict WHERE `alldistrict`.`id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":setDistrict" => $this->district, ":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is updated successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Error Occurred.
                                   </div>");
            header("Location: index.php");
        }

    }
    public function delete(){
        $sql="DELETE FROM `alldistrict` WHERE `id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is deleted successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Data is not deleted.
                                   </div>");
            header("Location: index.php");
        }
    }
    public function trash()
    {
        $sql = "UPDATE `alldistrict` SET `isTrash`=1 WHERE `alldistrict`.`id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is Trashed successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Error Occurred.
                                   </div>");
            header("Location: index.php");
        }
    }

    public function recycleBin()
    {
        $sql = "SELECT * FROM `alldistrict` WHERE `isTrash`=1";
        $getSql = $this->conn->query($sql);
        $result = $getSql->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function restore()
    {
        $sql = "UPDATE `alldistrict` SET `isTrash`=0 WHERE `alldistrict`.`id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is restored successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Error Occurred.
                                   </div>");
            header("Location: index.php");
        }
    }
}
