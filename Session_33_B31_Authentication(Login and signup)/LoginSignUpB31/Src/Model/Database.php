<?php

namespace App\Model;
use PDO;
class Database
{
    public $serverName = "localhost";
    public $databaseName = "loginsignupb31";
    public $user = "root";
    public $pass = "";
    public $conn;


    public function __construct()
    {
        try {
            # MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=$this->serverName;dbname=$this->databaseName", $this->user, $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

}