<?php
namespace App\Users;
if(!isset($_SESSION['user_emailb31'])){
    session_start();
}
use App\Model\Database as DB;
use PDO;
class Auth extends DB
{

    public $email="";


    public function setData($data = Array())
    {
        if (array_key_exists('first_name', $data)) {
            $this->first_name = $data['first_name'];
        }
        if (array_key_exists('last_name', $data)) {
            $this->last_name = $data['last_name'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = md5($data['password']);
        }
        if (array_key_exists('token', $data)) {
            $this->token = ($data['token']);
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        return $this;
        //var_dump($this);
        //die();
    }





    public function isRegistered(){
        $query="SELECT * FROM `users` WHERE `email`=:email";
        $stmt = $this->conn->prepare($query);
        $stmt->execute(array(
            ":email" => $this->email,
        ));
        $_singleInfo= $stmt->fetchAll(PDO::FETCH_ASSOC);
        //echo count($_singleInfo);
        //die();
        if(count($_singleInfo)>0){
            return TRUE;
        }else{
            return FALSE;
        }

    }
    public function is_valid_token(){
        $query="SELECT * FROM `users` WHERE `token`=:token";
        $stmt = $this->conn->prepare($query);
        $stmt->execute(array(
            ":token" => $this->token));
        $_singleInfo= $stmt->fetchAll(PDO::FETCH_ASSOC);
        //echo count($_singleInfo);
        //die();
        if(count($_singleInfo)>0){
            return TRUE;
        }else{
            return FALSE;
        }

    }
    public function setActive(){
        $query="UPDATE `users` SET `is_active` = 'active' WHERE `users`.`token` = :token";
        $stmt = $this->conn->prepare($query);
        $result=$stmt->execute(array(
            ":token" => $this->token));
        if($result){
            return TRUE;
        }else{
            return FALSE;
        }

    }

    public function isValidUser(){
        $query="SELECT * FROM `users` WHERE `email`=:email AND `password`=:password";
        $stmt = $this->conn->prepare($query);
        $stmt->execute(array(
            ":email" => $this->email,
            ":password"=>$this->password
        ));
        $_singleInfo= $stmt->fetchAll(PDO::FETCH_ASSOC);
        //echo count($_singleInfo);
        //die();
        if(count($_singleInfo)>0){
            return TRUE;
        }else{
            return FALSE;
        }

    }
    public function isActive(){
        $query="SELECT * FROM `users` WHERE `email`=:email AND `password`=:password AND `is_active`='active'";
        $stmt = $this->conn->prepare($query);
        $stmt->execute(array(
            ":email" => $this->email,
            ":password"=>$this->password

        ));
        $_singleInfo= $stmt->fetchAll(PDO::FETCH_ASSOC);
        //echo count($_singleInfo);
        //die();
        if(count($_singleInfo)>0){
            return TRUE;
        }else{
            return FALSE;
        }

    }

    public function is_loggedin(){
        if((array_key_exists('user_emailb31',$_SESSION))&& (!empty($_SESSION['user_emailb31']))){
            return TRUE;
        }
        else{
            return FALSE;
        }

    }
    public function logout()
    {
        if ((array_key_exists('user_emailb31', $_SESSION)) && (!empty($_SESSION['user_emailb31']))) {
            $_SESSION['user_emailb31'] = "";
            return TRUE;
        }

    }








}