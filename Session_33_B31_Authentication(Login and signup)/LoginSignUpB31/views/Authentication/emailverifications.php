<?php
//var_dump($_GET);
//die();
include_once('../../vendor/autoload.php');
session_start();
use App\Users\User;
use App\Users\Auth;
use App\Message\Message;
use App\Utility\Utility;

//var_dump($_POST);
//die();
//Utility::dd($_POST);

$obj= new Auth();
$result=$obj->setData($_GET)->is_valid_token();
if($result){
    $res=$obj->setData($_GET)->setActive();
    if($res){
        Message::message("Account has been activated");
        Utility::redirect('../../index.php');
    }else{
        Message::message("Some error has been occured while activating account");
        Utility::redirect('../../index.php');
    }

} else{
    Message::message("Not Valid Token");
    Utility::redirect('../../index.php');
}