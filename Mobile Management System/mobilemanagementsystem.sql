-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2016 at 07:06 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mobilemanagementsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`) VALUES
(1, 'imamul', 'imamul68e@gmail.com'),
(2, 'sdfgh', 'ulfgggfd68e@gmail.com'),
(3, 'kjhgfd', 'uytrfd@gmail.com'),
(4, 'qfg', 'ertghfd'),
(5, 'eyuiopiouytre', 'ykjhgf'),
(6, 'ytt', 'ytret'),
(7, 'wertyu', 'ertyuil;\''),
(8, 'wertyu', 'ertyuil;\''),
(9, 'wertyu', 'ertyuil;\''),
(10, 'IEB : The Institution of Engineers, Bangladesh', 'fghhggfd'),
(11, 'imam', 'ertyuil;\''),
(12, 'IEB : The Institution of Engineers, Bangladesh', 'ertyuil;\''),
(13, 'IEB ', 'imamul68e@gmail.com'),
(14, 'wertyu', 'ytret'),
(15, 'rtyuiiuytrt', 'yuklkjhjklkjhg'),
(16, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mobile`
--

CREATE TABLE `mobile` (
  `id` int(11) NOT NULL,
  `mobileTitle` varchar(255) NOT NULL,
  `createdBy` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile`
--

INSERT INTO `mobile` (`id`, `mobileTitle`, `createdBy`) VALUES
(1, 'Nokia', 'Imamul'),
(2, 'samsung', 'shuvo'),
(3, 'Motorolla', 'kamal'),
(4, 'Motorolla', 'kamrul'),
(5, 'sony', 'Imamul'),
(6, 'Amanr', 'Bari'),
(7, 'Nokia', 'Imamul'),
(9, 'Motor', 'Amar to hoyni'),
(10, 'sdfgh', 'dfgbn'),
(11, 'Nokia', 'Bari'),
(12, 'Motorolla', 'kamal'),
(13, 'Motor', 'Imamul'),
(14, 'samsung', 'Imamul'),
(15, 'erwwtrt', 'tyyty'),
(16, 'Motorolla', 'Imamul'),
(17, 'Motorolla', 'Amar to hoyni'),
(18, 'Nokia', 'dfgbn'),
(19, 'Nokia', 'fd'),
(20, 'Nokia', 'fd'),
(21, 'Motorolla', 'dfgbn'),
(22, 'Symphony', 'Imamul'),
(23, 'Motorolla', 'fd'),
(24, 'Motorollaasdfghm,', 'sdfghjkl;'),
(25, 'amajfdj', 'gfgd'),
(26, 'fff', 'frrr'),
(27, 'asdf', 'qwert'),
(28, 'sfdghj', 'fgfhjh'),
(29, 'gfhgg', 'ghgjghgr'),
(30, 'Motorolla', 'Bari'),
(31, 'Motorolla', 'Bari');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile`
--
ALTER TABLE `mobile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `mobile`
--
ALTER TABLE `mobile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
