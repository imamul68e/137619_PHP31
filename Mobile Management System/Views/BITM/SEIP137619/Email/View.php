<?php
include("../../../../vendor/autoload.php");

use App\BITM\SEIP137619\Email\Email;

$email = new Email();
$email->setData($_GET);
$singleResult = $email->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Single User's Email</h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleResult['id']?></li>
        <li class="list-group-item">Name: <?php echo $singleResult['name']?></li>
        <li class="list-group-item">Email: <?php echo $singleResult['email']?></li>
    </ul>
</div>

</body>
</html>


