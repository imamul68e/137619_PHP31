<?php
include($_SERVER['DOCUMENT_ROOT'] . "/137619_PHP31/Mobile Management System/vendor/autoload.php");
session_start();
use App\BITM\SEIP137619\Mobile\Mobile;
use App\BITM\SEIP137619\Message\Message;

$mobile = new Mobile();
$allResult = $mobile->setData($_GET)->view();

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Mobile Update Form</h2>
    <form method="post" action="Update.php">
        <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $allResult['id']?>">
            <label for="title">Title</label>
        <input type="text" class="form-control" name="title" id="title" value="<? echo $allResult['mobileTitle'] ?>">
        </div>
        <div class="form-group">
            <label for="createdBy">Created By</label>
            <input type="text" class="form-control" name="createdBy" id="createdBy" value="<? echo $allResult['createdBy'] ?>">
        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>
</body>
</html>

