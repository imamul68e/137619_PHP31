<?php
include($_SERVER['DOCUMENT_ROOT'] . "/137619_PHP31/Mobile Management System/vendor/autoload.php");
use App\BITM\SEIP137619\Mobile\Mobile;


$mobile = new Mobile();
$mobile->setData($_GET);
$result = $mobile->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mobile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>Single Mobile Informations:</h2>
    <table class="table table-hover table-striped table-bordered">
        <tr>
            <td><strong>ID</strong></td>
            <td><?php echo $result['id']; ?></td>

        </tr>
        <tr>
            <td><strong>Mobile Brand</strong></td>
            <td><?php echo $result['mobileTitle']; ?></td>
        </tr>
        <tr>
            <td><strong>Created By</strong></td>
            <td><?php echo $result['createdBy']; ?></td>
        </tr>
    </table>
</div>
</body>
</html>
