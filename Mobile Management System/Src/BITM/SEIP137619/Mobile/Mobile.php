<?php
namespace App\BITM\SEIP137619\Mobile;
use App\BITM\SEIP137619\Message\Message;
use PDO;
class Mobile
{
    public $id = "";
    public $mobileTitle = "";
//    public $created = "";
    public $createdBy = "";
//    public $modified = "";
//    public $modifiedBy = "";
//    public $deletedAt = "";
    public $serverName = "localhost";
    public $dabName = "mobilemanagementsystem";
    public $user = "basis";
    public $pass = "bitm";
    public $conn;

    public function __construct($model = false)
    {
        try {
            $host = $this->serverName;
            $databname = $this->dabName;
            $user = $this->user;
            $pass = $this->pass;
            $this->conn = new PDO("mysql:host=$host;dbname=$databname", $user, $pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data ="")
    {
        if (array_key_exists('title', $data) and !empty($data)) {
            $this->mobileTitle = $data['title'];
        }
        if (array_key_exists('createdBy', $data) and !empty($data)) {
            $this->createdBy = $data['createdBy'];
        }
        if(array_key_exists('id',$data) and !empty($data)){
            $this->id=$data['id'];
        }
        return $this;
    }

    public function store()
    {
        $sql = "INSERT INTO `mobile` (`mobileTitle`, `createdBy`) VALUES (:getTitle, :whoCreated)";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(':getTitle' => $this->mobileTitle, ':whoCreated' => $this->createdBy));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is inserted successfully.
                                   </div>");
            header("Location: Index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Data is not inserted successfully.
                                   </div>");
            header("Location: Index.php");
        }

    }

    public function index()
    {
        $sql = "SELECT * FROM `mobile`";
        $getSql = $this->conn->query($sql);
        $allResult = $getSql->fetchAll(PDO::FETCH_ASSOC);
        return $allResult;
    }
    public function view(){
        $sql="SELECT * FROM `mobile` WHERE `id`=:setid";
        $stmt=$this->conn->prepare($sql);
        $stmt->execute(array(':setid'=>$this->id));
        $singleData=$stmt->fetch(PDO::FETCH_ASSOC);
        return $singleData;
    }
    public function update()
    {
        $sql="UPDATE `mobile` SET `mobileTitle` = :mobTitle, `createdBy` = :whoCreates WHERE `mobile`.`id` = :getId";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(':mobTitle' => $this->mobileTitle, ':whoCreates' => $this->createdBy, ':getId'=>$this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is updated successfully.
                                   </div>");
            header("Location: Index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Data is not updated.
                                   </div>");
            header("Location: Index.php");
        }

    }

    public function delete()
    {
        $sql="DELETE FROM `mobile` WHERE `mobile`.`id` = :getId";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(':getId'=>$this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is deleted successfully.
                                   </div>");
            header("Location: Index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Data is not deleted.
                                   </div>");
            header("Location: Index.php");
        }

    }
}