<?php
echo strip_tags("Hello <b><i>world!</i></b>","<b>")."<br>";     // optional value <b>, so world becomes bold and other tags will be skipped
echo strip_tags("Hello <b><i>world!</i></b>");
?>