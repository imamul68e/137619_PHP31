
<?php
    $str = "Is,your,name,Imamul?";
    var_dump(explode(',',$str));    // not using limit
echo "-----------------------------------------------";
    echo "<br>";

    $name="My name is Imamul.";
    $n=explode(' ', $name);     // not using limit
    echo $n[1].'='.$n[3];
    echo "<br>";

echo "-----------------------------------------------";
var_dump(explode(",",$str,0 ));        // using limit and if limit=1(+ve) then it shows one array by default.
echo "<br>";
var_dump(explode(",",$str,1 ));     //shows 1 array
echo "<br>";
var_dump(explode(",",$str,2 ));     // shows 2 arrays
echo "<br>";
var_dump(explode(",",$str,3 ));
echo "<br>";
var_dump(explode(",",$str,4 ));
echo "<br>";
var_dump(explode(",",$str,5 ));       // shows 4 arrays because $str hsa only 4 elements.

echo "-----------------------------------------------";
echo "<br>";
var_dump(explode(",",$str,-1 ));        // -ve limit
echo "<br>";
var_dump(explode(",",$str,-2 ));        // -ve limit
echo "<br>";
var_dump(explode(",",$str,-3 ));        // -ve limit
echo "<br>";
var_dump(explode(",",$str,-4 ));        // -ve limit
echo "-----------------------------------------------";
echo "<br>";


$fullname="Md.:Imamul:Hossain:Kalaroa:Satkhira";
list($first_name, $mid_name, $last_name, $thana, $dist)=explode(':', $fullname);
echo $mid_name.'('. $dist.')';

?>
