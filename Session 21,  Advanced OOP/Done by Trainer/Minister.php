<?php

abstract class Minister
{
    public abstract function getPower();

    protected abstract function setPower();

    public function food()
    {
        echo "yes";
    }


}

class Police extends Minister
{
    public function getPower()
    {
        echo "OWNER OF Bangladesh";

    }

    public function setPower()
    {

    }

    public function vip()
    {

    }
}

$obj = new Police();
$obj->food();