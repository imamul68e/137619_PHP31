<?php

class cricket
{
    public function __construct()
    {
        echo "This is a constructor function<br>";
    }
}
class Movie extends cricket{
    public function __construct()
    {
        parent:: __construct();
        echo "This is child class of cricket";
    }
}