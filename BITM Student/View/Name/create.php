<?php
include("../../vendor/autoload.php");
session_start();
use App\Message\Message;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Student Info</h2>
    <div id="message">
        <?php
            if (array_key_exists('giveMessage',$_SESSION) and !empty($_SESSION['giveMessage'])){
            echo Message::message();
            }
        ?>
    </div>
    <form action="store.php" method="post">
        <div class="form-group">
            <label for="seid">SEID:</label>
            <input type="number" class="form-control" id="seid" name="seid" placeholder="Enter SEID">
        </div>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
<script>
    $("#message").show().delay(3000).fadeOut();
</script>
</body>
</html>