<?php
namespace App\Name;

use App\Message\Message;
use PDO;

class Name
{
    public $id = '';
    public $name = '';
    public $seid = '';
    public $host = 'localhost';
    public $user = 'ami';
    public $dbName = 'bitmstudent';
    public $pass = 'ami';
    public $conn;

    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql: host=$this->host; dbname=$this->dbName", $user = $this->user, $pass = $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data = "")
    {
        if (array_key_exists('seid', $data) and !empty($data)) {
            $this->seid = $data['seid'];
        }
        if (array_key_exists('name', $data) and !empty($data)) {
            $this->name = $data['name'];
        }
        return $this;
    }

    public function store()
    {
        $sql = "INSERT INTO `student`(`seid`, `name`) VALUES (:setseid,:setname)";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(':setseid' => $this->seid, ':setname' => $this->name));
        if ($result) {
            echo Message::message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Successfully inserted.
                                  </div>");
           header("Location: create.php");
        } else {
            echo Message::message("<div class=\"alert alert-warning\">
                                        <strong>Warning!</strong> Unsuccessfully.
                                  </div>");
            header("Location: create.php");
        }
    }
}