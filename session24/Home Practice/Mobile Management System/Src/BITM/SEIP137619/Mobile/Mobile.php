<?php
namespace App\BITM\SEIP137619\Mobile;
use PDO;
class Mobile
{
    public $id = "";
    public $mobileTitle = "";
//    public $created = "";
    public $createdBy = "";
//    public $modified = "";
//    public $modifiedBy = "";
//    public $deletedAt = "";
    public $serverName = "localhost";
    public $dbName = "mobilemanagementsystem";
    public $user = "basis";
    public $pass = "bitm";
    public $conn;

    public function __construct($model = false)
    {
        try {
            $host = $this->serverName;
            $databname = $this->dbName;
            $user = $this->user;
            $pass = $this->pass;
            $this->conn = new PDO("mysql:host=$host;dbname=$databname", $user, $pass);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function setData($data=""){
        if(array_key_exists('title', $data) and !empty($data)){
            $this->mobileTitle=$data['title'];
        }
        if(array_key_exists('createdBy', $data) and !empty($data)){
            $this->createdBy=$data['createdBy'];
        }
        return $this;
    }
//    public function index()
//    {
//        echo "hai hai kore ki kore ki!!!!";
//    }

//    public function create()
//    {
//
//    }

    public function store()
    {
        $query="INSERT INTO `mobile` (`mobileTitle`, `createdBy`) VALUES (:getTitle, :whoCreated)";
        $getQuery= $this->conn->prepare($query);
        $result=$getQuery->execute(array(':getTitle'=>$this->mobileTitle, ':whoCreated'=>$this->createdBy));
        if ($result){
            echo "Data is inserted successfully.";
        }
        else{
            echo "Data is not inserted.";
        }
    }

//    public function edit()
//    {
//
//    }
//
//    public function update()
//    {
//
//    }
//
//    public function delete()
//    {
//
//    }
}