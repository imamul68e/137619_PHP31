<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Mobile Form</h2>
    <form method="post" action="Store.php">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" id="email" placeholder="Enter Mobile Name">
        </div>
        <div class="form-group">
            <label for="createdBy">Created By</label>
            <input type="text" class="form-control" name="createdBy" id="createdBy" placeholder="Who has created?">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
</body>
</html>

