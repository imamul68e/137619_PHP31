<?php
include("../../../vendor/autoload.php");
use App\BITM\PersonalInfo\PersonalInfo;

$person = new PersonalInfo();
$person->setData($_GET);
$allResult=$person->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Personal Info</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
        <div class="row">
            <div class="col-md-7 col-md-offset-2">
                <h2>Personal History Form</h2>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <li class="form-control"><?php echo $allResult['name']?></li>
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <li class="form-control"><?php echo $allResult['email']?></li>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-md-offset-2">
                <div class="form-group">
                    <label for="gender">Gender:</label><br>
                    <li class="form-control"><?php echo $allResult['gender']?></li>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-1">
                <div class="form-group">
                    <label>Hobby:</label><br>
                    <li class="form-control"><?php echo $allResult['hobby']?></li>
                </div>
            </div>
        </div>
    <a href="create.php?id=<?php echo $allResult['id']; ?>" class="btn btn-primary btn-xs"
       role="button">Add New</a>
    <a href="edit.php?id=<?php echo $allResult['id']; ?>" class="btn btn-info btn-xs"
       role="button">Edit</a>
    <a href="trash.php?id=<?php echo $allResult['id']; ?>" class="btn btn-warning btn-xs"
       role="button">Trash</a>
    <form action="delete.php" method="post">
        <input type="hidden" name="id" value="<?php echo $allResult['id']; ?>">
        <button class="btn btn-danger btn-xs">Delete</button>
    </form>
</div>
</body>
</html>
