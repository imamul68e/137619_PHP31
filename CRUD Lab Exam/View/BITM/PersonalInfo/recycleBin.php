<?php
include("../../../vendor/autoload.php");
use App\BITM\PersonalInfo\PersonalInfo;

$person = new PersonalInfo();
$person->setData($_POST);
$allResult = $person->recycleBin();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Recycle Bin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container"><br>
    <a href="index.php" class="btn btn-primary" role="button">All List</a>
    <h2>All Information</h2>
    <table class="table table-bordered table-striped table-hover table-responsive">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Hobby</th>
            <th>Actions</th>
        </tr>
        </thead>
        <?php
        $sl = 0;
        foreach ($allResult as $result) {
            $sl++; ?>
            <tbody>
            <tr>
                <td><?php echo $sl ?></td>
                <td><?php echo $result['id'] ?></td>
                <td><?php echo $result['name'] ?></td>
                <td><?php echo $result['email'] ?></td>
                <td><?php echo $result['gender'] ?></td>
                <td><?php echo $result['hobby'] ?></td>
                <td>
                    <a href="restore.php?id=<?php echo $result['id']; ?>" class="btn btn-primary btn-xs"
                       role="button">Restore</a>
                    <a href="delete.php?id=<?php echo $result['id']; ?>" class="btn btn-danger btn-xs"
                       role="button">Delete</a>
                </td>
            </tr>
            </tbody>
        <?php } ?>
    </table>
</div>
</body>
</html>

