<?php
namespace App\BITM\PersonalInfo;

use PDO;

class PersonalInfo
{
    public $id = "";
    public $name = "";
    public $email = "";
    public $hobby = "";
    public $gender = "";
    public $user = "atomic";
    public $pass = "123";
    public $hostName = "localhost";
    public $db = "personalinfo";
    public $conn;

    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->hostName;dbname=$this->db", $this->user, $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data = '')
    {
        if (array_key_exists('name', $data) and !empty($data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('email', $data) and !empty($data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('gender', $data) and !empty($data)) {
            $this->gender = $data['gender'];
        }
        if (array_key_exists('Hobby', $data) and !empty($data)) {
            $this->hobby = $data['Hobby'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
    }

    public function store()
    {
        $sql = "INSERT INTO `personalinfo`(`name`, `email`, `gender`, `hobby`, `isTrash`) VALUES (:setName,:setEmail,:setGender,:setHobby,:setTrash)";
        $sqlResult = $this->conn->prepare($sql);
        $result = $sqlResult->execute(array(":setName" => $this->name, ":setEmail" => $this->email, ":setGender" => $this->gender, ":setHobby" => $this->hobby, ":setTrash" => 0));
        if($result){
            echo "Data is stored successfully.";
            header("Location: index.php");
        }
        else{
            echo "Some error occurred.";
            header("Location: index.php");
        }
    }

    public function index()
    {
        $sql = "SELECT * FROM `personalinfo` WHERE `isTrash`=0";
        $sqlResult = $this->conn->query($sql);
        $Result = $sqlResult->fetchAll(PDO::FETCH_ASSOC);
        return $Result;
    }

    public function view()
    {
        $sql = "SELECT * FROM `personalinfo` WHERE `id`=:id";
        $sqlResult = $this->conn->prepare($sql);
        $result = $sqlResult->execute(array(":id" => $this->id));
        $Result = $sqlResult->fetch(PDO::FETCH_ASSOC);
        return $Result;
    }

    public function update()
    {
        $sql = "UPDATE `personalinfo` SET `name` = :setName, `email` = :setEmail, `gender` = :setGender, `hobby` = :setHobby WHERE `personalinfo`.`id` = :id";
        $sqlResult = $this->conn->prepare($sql);
        $result = $sqlResult->execute(array(":setName" => $this->name, ":setEmail" => $this->email, ":setGender" => $this->gender, ":setHobby" => $this->hobby, ":id" => $this->id));
        if($result){
            echo "Data is updated successfully.";
            header("Location: index.php");
        }
        else{
            echo "Some error occurred.";
            header("Location: index.php");
        }
    }

    public function trash()
    {
        $sql = "UPDATE `personalinfo` SET `isTrash` = '1' WHERE `personalinfo`.`id` = :id";
        $sqlResult = $this->conn->prepare($sql);
        $result = $sqlResult->execute(array(":id" => $this->id));
        if($result){
            echo "Data is trashed successfully.";
            header("Location: index.php");
        }
        else{
            echo "Some error occurred.";
            header("Location: index.php");
        }
    }

    public function recycleBin()
    {
        $sql = "SELECT * FROM `personalinfo` WHERE `isTrash`=1";
        $sqlResult = $this->conn->query($sql);
        $Result = $sqlResult->fetchAll(PDO::FETCH_ASSOC);
        return $Result;
    }

    public function restore()
    {
        $sql = "UPDATE `personalinfo` SET `isTrash` = '0' WHERE `personalinfo`.`id` = :id";
        $sqlResult = $this->conn->prepare($sql);
        $result = $sqlResult->execute(array(":id" => $this->id));
        if($result){
            echo "Data is restored successfully.";
            header("Location: recycleBin.php");
        }
        else{
            echo "Some error occurred.";
            header("Location: index.php");
        }
    }

    public function delete()
    {
        $sql="DELETE FROM `personalinfo` WHERE `personalinfo`.`id` = :id";
        $sqlResult = $this->conn->prepare($sql);
        $result = $sqlResult->execute(array(":id" => $this->id));
        if($result){
            echo "Data is deleted successfully.";
            header("Location: index.php");
        }
        else{
            echo "Some error occurred.";
            header("Location: index.php");
        }
    }
}