<?php
session_start();
//echo $_SERVER['DOCUMENT_ROOT'];
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."AtomicProjectB31".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");


use App\Bitm\SEIPXXXX\Mobile\Mobile;
use App\Bitm\SEIPXXXX\Message\Message;

$obj= new Mobile();
$allResult=$obj->trashed();
//\App\Bitm\SEIPXXXX\Utility\Utility::dd($allResult);
//App\Bitm\SEIPXXXX\Utility\Utility::dd($allResult);


?>


<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
    </br>
    </br>
    <div id="message">
        <?php
        if(array_key_exists('success_message',$_SESSION) and !empty($_SESSION['success_message'])){
            echo Message::message();
        }
        ?>
    </div>

    <a href="index.php" class="btn btn-primary" role="button">All data list</a>





    <h2>All trashed List</h2>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Mobile Name</th>
            <th>Model</th>
            <th>Deleted_at</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl=0;
        foreach($allResult as $result){
            $sl++?>
            <tr>
                <td><?php echo $sl?></td>
                <td><?php echo $result->id ?></td>
                <td><?php echo $result->title ?></td>
                <td><?php echo $result->model_name ?></td>
                <td><?php echo $result->deleted_at ?></td>
                <td>
                    <a href="delete.php?id=<?php echo $result->id?>" class="btn btn-danger" role="button">Delete</a>
                    <a href="restore.php?id=<?php echo $result->id?>" class="btn btn-primary" role="button">Restore</a>

                    <!--                    <form method="post" action="delete.php">-->
                    <!--                        <input type="hidden" name="id" value="--><?php //echo $result->id ?><!--" >-->
                    <!---->
                    <!--                        <button type="submit" Onclick="return ConfirmDelete()">Delete</button>-->
                    <!--                    </form>-->


                </td>
            </tr>
        <?php } ?>

        </tbody>
    </table>
</div>
<script>
    $("#message").show().delay(3000).fadeOut();

    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>

</body>
</html>
