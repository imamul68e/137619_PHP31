<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Mobile add form</h2>
    <form action="store.php" method="post">
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" class="form-control" id="title" name="title"
placeholder="Enter your mobile name">
        </div>
        <div class="form-group">
        <label for="model_name">Model:</label>
        <input type="text" class="form-control" id="model_name" name="model_name"
               placeholder="Enter your mobile model">
</div>


        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
</div>

</body>
</html>
