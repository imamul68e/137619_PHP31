<?php
abstract class CourseManagement
{
    abstract function getCourse();
    abstract function setCourse();
}
class Course extends CourseManagement{
    public function getCourse(){
       echo "This is my Course";
    }
    public function setCourse(){
       echo $this->getCourse();
    }
}
$obj= new Course();
$obj->setCourse('PHP');


