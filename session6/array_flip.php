<?php
$array=array('a','b','c');
var_dump(array_flip($array));

$arr = array('a'=> 1, 'b'=> 2);
echo "<pre>";
print_r(array_flip($arr));

$a = array('a' => '1', 'b'=> '2');
echo "<pre>";
var_dump(array_flip($a));

$arrayName = array('a' => 1 , 'b'=> 1, 'c'=> 2);
var_dump(array_flip($arrayName));